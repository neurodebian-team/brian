#!/bin/bash
set -e

pkg=brian

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

# Return exit code 1 if any of the listed package in argument is not available.
is_installed () {
	local pkg
	for pkg in "$@"
	do
		if dpkg --status "$pkg" 2>/dev/null \
		   | grep -q 'Status: install ok installed'
		then continue
		else return 1
		fi
	done
}

cd "${AUTOPKGTEST_TMP}"

# Check for test dependencies
if ! is_installed python3-pytest
then
	cat >&2 <<-END
	error: running the unit test requires installing python3-pytest
	END
	exit 1
fi

if ! is_installed python3-sphinx python3-dev g++
then
	cat >&2 <<-END
	warning: parts of the unit test suite require the following packages:
	  - python3-sphinx
	  - python3-dev
          - g++
	END
fi

mkdir home
export HOME="${AUTOPKGTEST_TMP}/home"
python3 -c 'import brian2; exit(not brian2.test())'
